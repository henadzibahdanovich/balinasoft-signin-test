//
//  ServerManager.swift
//  BalinaSoft-test
//
//  Created by Gena Bogdanovich on 05.01.2019.
//  Copyright © 2019 Gena Bogdanovich. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class ServerManager {

    static var shared = ServerManager()
    
    func chekDomen(domen: String ,_ onCompletion: @escaping (Bool) -> Void) {
        
        var chekedDomen = domen

        let urlToString = URL(string: "https://\(chekedDomen)/")
        
        guard let url = urlToString else {return}
        
        Alamofire.request(url)
            .responseString { response in
                print("Success: \(response.result.isSuccess)")
                var statusCode = response.response?.statusCode
                if let error = response.result.error as? AFError {
                    
                    statusCode = error._code // statusCode private
                }
                print(statusCode as Any) // the status code
                if statusCode == 200 {
                    onCompletion(true)
                } else {
                    onCompletion(false)
                }
        }
        
    }
    
}
